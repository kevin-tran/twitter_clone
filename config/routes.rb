Rails.application.routes.draw do
  # get 'password_resets/new'

  # get 'password_resets/edit'

  #~ get 'sessions/new'
  #~ get 'users/new'
  #** must comment out since we're setting root page to home anyways
  # get 'static_pages/home'


  # You can have the root of your site routed with "root"
  root 'static_pages#home'
  #** This changes the URL static_pages/home to the controller/action pair static_pages#home
  #** which ensures that the GET requests for / get routed to the home action in the Static Pages controller
  
  
  get 'help'    => 'static_pages#help'
  get 'about'   => 'static_pages#about'
  get 'contact' => 'static_pages#contact'
  get 'signup' => 'users#new'
  #** [ 5.3.2:22 ] Routes for static pages
  #** routes a GET request for the URL /help to the help action in the static
  #** Pages controller, so that we can use the URL /help in place of /static_pages/help
  #** 2 named routes get created, help_path + help_url
  # [ 5.3.4:33 -> home.html.erb] Add named route for signup page
  
  
  get    'login'   => 'sessions#new'
  post   'login'   => 'sessions#create'
  delete 'logout'  => 'sessions#destroy'
  #                 [ 8.1.1:1 -> sessions/new.html.erb ]
  # Unlike the Users resource, which used the special resources method to obtain
  # a full suite of RESTful routes automatically (index, show, new, create, edit
  # update, destroy)
  # The sessions resources will only use named routes, handling GET and POST
  # requests witht he login route and DELETE request with the logout route
  
  
  resources :users do
    member do
      get :following, :followers
    end
  end
  # [ 12.2.2 -> shared/_stats.html.erb]
  # adding a following and followers actions to the Users controller
  
  
  

  resources :users
  #                  [7.1.2:3 -> show.html.erb] 
  # This endows our app with all actions needed for our RESTful Users 
  # resource, along with a large number of named routes. 
  # At this point, since most of these actions (edit, delete, update, etc)
  # actions for the Users controller hasn't been made yet, we are going to get an error
  

  resources :account_activations, only: [:edit]
  # [ 10.1.1:1 -> [timestamp]_add_activation_to_users.rb ] Adding resource for account activations
  # because the activation link will be modifying the user's activatiions tatus,
  #we'll plan to use the edit action. this requires an Account Actv controller
  # This line will set a named route for the edit action
  
  
  
  resources :password_resets,     only: [:new, :create, :edit, :update]
  # [ 10.2.1:37 -> sessions/new.html.erb ] adding resource for password resets
  # bc we'll need forms both for creating new password resets and for updating
  # them by changing the password in the User model, we need routes for
  # new, create, edit, and update
  
  
  resources :microposts,          only: [:create, :destroy]
  # [ 11.3.1:29 -> microposts_controller_test.rb ] routes for Microposts resources
  # Provides two RESTful routes, create and destroy.
  
  
  resources :relationships,       only: [:create, :destroy]
  # [ 12.2.2:20 -> views/user/_follow.html.erb]
  # Adding routes for user relationships, similar to microposts
  
end
