class User < ActiveRecord::Base
   has_many :microposts, dependent: :destroy
   #** [ 11.1.4:18 -> user_test.rb ] ensuring microposts desroyed along iwth user
   #** dependent: :destroy arranges for dependent microposts to be destroyed
   #** whent he user itself is destroyed
   #** [ 11.1.3:10 -> micropost_test.rb ] user has many microposts
   # 2) in the case of the user/micropost association, has_many :microposts works
   # bc by convention, Rails looks for a micropost model correspondnig to the #
   # :microposts symbol
   
   
   has_many :active_relationships, class_name: "Relationship",
                                   foreign_key: "follower_id",
                                   dependent: :destroy
   # [ 12.1.1:2 -> user_test.rb ] 
   # 1) Implementing active relationships has_many assoc
   # 2)
   # 3) treat following as an array
  
  has_many :passive_relationships, class_name:  "Relationship",
                                   foreign_key: "followed_id",
                                   dependent:   :destroy
   # [ 12.1.5:12 -> user_test.rb ] 
   # Implementing user.followers using passive relationships
   
                                   
   has_many :following, through: :active_relationships, source: :followed   
   # 12.1.4:8 -> ] 
   # Adding hte user moder following association
   # 1) a user has many following THROUGH relationships. EXPL MORE 
   
   has_many :followers, through: :passive_relationships, source: :follower
   
   attr_accessor :remember_token, :activation_token, :reset_token
   before_save :downcase_email
   #** ~ this is a callback, a special method that gets invoked at a particular
   #** point in the lifecycle of an active record object
   #** ~ we could have written: self.email = self.email.downcase
   #** self refers to the current user, but inside the User model the self 
   #** keyword is optional on the right hand side.
   #** ~ A block is passed to the before_save callback and sets the user's email
   #** address to a lower-case version
   before_create :create_activation_digest
   # happens before the user has been created
   # purpose of callback is to assign token and corresponding digest
   # as a result, when a new user is defined, it will automatically get
   # activation token and digest attributes

   validates :name, presence: true, length: { maximum: 50 }
   #** validates is a method
   #** presence: true is a one element options hash, thus curly braces are
   #** optional when passing hashes as final argument
   #** equivalently, validates(:name, presence: true)
   
    VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
   validates :email, presence: true, length: { maximum: 255 },
                       format: { with: VALID_EMAIL_REGEX },
                       uniqueness: { case_sensitive: false }
   #** format: only email of a certain format                        
   #** uniqeuness: only one of this may exist inside the DB BROOOOO!
   #** uniqueness accepts the option case_sensitive option and infers that uniqueness
   #** should be true as well
   
   validates :password, length: { minimum: 6 }, allow_blank: true
   #** [ 9.1.4:10 (SKIP 11) -> users_controller.rb ] Allowing blank passwords on update
   #** has_secure_password enforces presence validatons upon object creation
   #** so allow_blank will only apply to when updating.
   
   has_secure_password
   #** 1) save a securely hashed password_digest attr to the DB
   #** 2) a pair of virtual attributes (password and password_confirmation), including
   #** presence validation upon obj creation and matching validation
   #** 3) an authenticate method that returns the user when the password is correct
   #** and false otherwise
         #** It computes the digest of the password we input and then match it
         #** to the result of password_digest in the DB
         
         
   # Returns the hash digest of the given string.
  def User.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                  BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end
   # [ 8.2.4:18 -> test/fixtures/users.yml ] Adding digest method for use in fixtures


   # Returns a random token
   def User.new_token
      SecureRandom.urlsafe_base64
   end
      # [ 8.4.1:31 -> STAY ] Method for generating tokens
   # Create token then save to DB
   # As a general rule, methods that don't require an instance object should
   # be made a class method.
   
   
   # Remember a user in the database for use in persistent sessions.
   def remember
      self.remember_token = User.new_token
      update_attribute(:remember_digest, User.digest(remember_token))
   end
   # [ 8.4.1:32 -> STAY ] Adding remember method to user model.
   # 1) create a valid token and associated digest by first making a new
   # remember token using User.new_tken
   # 2) Then update hte remember digest wth result of applying User.digest


   # Returns true if the given token matches the digest
   def authenticated?(attribute, token)
      digest = send("#{attribute}_digest")
      #** 1) Example of metaprogramming, program that writes a program.
      #** Send lets us call a method with a name of our choice by "sending a msg"
      #** to a given object
      #** 2) generalize method by adding fxn arg with name of digest then use interpol.
      #** 3) we're inside user model, so we can omit self.send(...)
      return false if digest.nil?
      BCrypt::Password.new(digest).is_password?(token)
   end
   # [8.4.2:33 -> sessions_controller.rb ] Adding an authenticated? method
   # [8.4.4:45 -> sessions/new.html.erb ] Update to handle nonexistent digestb
   # [ 10.1.3:24 (SKIP 25) -> sessions_helper.rb  ] generalized authenticated method
   
   
   
   
   # Forget a user.
   def forget
      update_attribute(:remember_digest, nil)
   end
   # [ 8.4.3:38 -> sessions_helper.rb ] adding a forget method
   # Update remember digest with nil
   
   
  # Activates an account.
  def activate
    update_attribute(:activated,    true)
    update_attribute(:activated_at, Time.zone.now)
  end
  #** update user's activation attributes

  # Sends activation email.
  def send_activation_email
    UserMailer.account_activation(self).deliver_now
  end
  # [ 10.1.4:33 -> users_controller.rb ] adding user activation methods to user model
  
  # Sets the password reset attributes.
  def create_reset_digest
    self.reset_token = User.new_token
    update_attribute(:reset_digest,  User.digest(reset_token))
    update_attribute(:reset_sent_at, Time.zone.now)
  end

  # Sends password reset email.
  def send_password_reset_email
    UserMailer.password_reset(self).deliver_now
  end
  # [ 10.2.1:42 -> user_mailer.rb ] adding password reset methods to the User model
  
    # Returns true if a password reset has expired.
  def password_reset_expired?
    reset_sent_at < 2.hours.ago
    # reads as " password reset sent less than two hours ago"
  end
  # [10.2.4:53 -> password_resets_test.rb ] adding pass reset methods
   
   
  # Defines a proto-feed.
  # See "Following users" for the full implementation.
  def feed
    following_ids = "SELECT followed_id FROM relationships
                     WHERE  follower_id = :user_id"
    Micropost.where("user_id IN (#{following_ids})
                     OR user_id = :user_id", user_id: id)
    # ? is to ensuire that id is properly escaped before being included in the 
    # underlying SQL query
  end
   # [ 11.3.3:44 -> static_pages_controller.rb ] 
   # Preliminary implmenetation for micropost status feed.
   # def feed
   #  microposts
   # end
 
 
  # Follows a user.
  def follow(other_user)
    active_relationships.create(followed_id: other_user.id)
  end

  # Unfollows a user.
  def unfollow(other_user)
    active_relationships.find_by(followed_id: other_user.id).destroy
  end

  # Returns true if the current user is following the other user.
  def following?(other_user)
    following.include?(other_user)
  end
  # [ 12.1.4:10 skip 11-> stay - GO UP]
  # Utility methods for following
 
 
   
   private
   
    # Converts email to all lower-case.
    def downcase_email
      self.email = email.downcase
    end

    # Creates and assigns the activation token and digest.
    def create_activation_digest
      self.activation_token  = User.new_token
      self.activation_digest = User.digest(activation_token)
    end   
   # [ 10.1.1:3 skip 4-11 -> account_activation.text.rb ] adding account activation code to user model
   
end
