class Relationship < ActiveRecord::Base
   # Gets created when generating a model
  belongs_to :follower, class_name: "User"
   # one that has a following
  belongs_to :followed, class_name: "User"
   # one of the following 

   # [ 12.1.2:3 -> relationship_test.rb ] 
   # Adding follower belongs_to association to Relationship mode
   
   validates :follower_id, presence: true
   validates :followed_id, presence: true
   # [ 12.1.3:5 skip 6,7 -> user.rb ]
   # adding relationship model validations
   
end
