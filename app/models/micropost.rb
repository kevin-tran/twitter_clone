class Micropost < ActiveRecord::Base
  belongs_to :user
  default_scope -> { order(created_at: :desc) }
  # 1) default_scope is used to set default order in which elements are retrieved from
  # the DB.
  # 2) include order argument which orders by created_at column in descending order
  # IE in descending order from newest to oldest
  # 3) -> is stabby lambda, it takes in a block and returns a Proc, which can 
  # then be evaluated with the call method.
  
  mount_uploader :picture, PictureUploader
  # To tell CarrierWave to associate image with a model is to use mount_uploader
  # which takes as arguments a symbol representing the attribute and the 
  # classname of the generated uploadrer
  
  validates :user_id, presence: true
  validates :content, presence: true, length: { maximum: 140 }
  validate :picture_size
  #** use validate as opposed to validates, to call a custom validation
  #** arranges to call the method corresponding to the given symbol

  private

    # Validates the size of an uploaded picture.
    def picture_size
      if picture.size > 5.megabytes
        errors.add(:picture, "should be less than 5MB")
      end
    end
  # Unlike previous model validations, file size validation doesn't correspond
  # to a built-in-rails validator. it requires a custom validation, which we'll call
  # picture size.
  
  
  

end

# [ 11.1.2:4 skip 5-> models/user.b ] validation for micropost's user_id
# [ 11.1.4:16 -> user.rb ] ordering microposts with defautl_scope