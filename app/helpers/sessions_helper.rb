module SessionsHelper
   
   def log_in(user)
      session[:user_id] = user.id
   end
   
   # Remembers a user in a persistent session.
   def remember(user)
      user.remember
      cookies.permanent.signed[:user_id] = user.id
      cookies.permanent[:remember_token] = user.remember_token
   end
   # =>           [ 8.4.2:35 -> STAY ] Remember the user
   # 1) Use signed method to securely encrypt cookie before placing on browser
   # 2) want user id to be paired with the permanent remember token
   # 
   # A user logging in will be rememrered in the sense that their browser
   # will get a valid remember token
   
   
   
  # Returns true if the given user is the current user.
  def current_user?(user)
    user == current_user
  end
   # [ 9.2.2:24 -> users_controller.rb ] the current_user? method
   # to be used in the correct_user before filter
   
   
   
   # Returns the user corresponding to the remember token cookie
   def current_user
      if (user_id = session[:user_id])
      # "If session of user id exists (while setting user id to session of user id)"
         @current_user ||= User.find_by(id: user_id)
         # knows only temporary session
         # Returns current logged in user, if any.
      elsif (user_id = cookies.signed[:user_id])
         # raise    # The tests still pass, so this bbanch is currently untested
         user = User.find_by(id: user_id)
         if user && user.authenticated?(:remember, cookies[:remember_token])
            log_in user
            @current_user = user
         end
      end
      # [ 8.4.2:36 (SKIP 37) -> user.rb ] Updating current_user for persistent sessions
      # [ 8.4.5:53 (Skip54) -> sesssions_helper_test.rb  ] Raise an exceptiion in an utested branch
      # [ 10.1.2:26 -> user_test.rb ] Using generalized authenticated method
      
      # equivalent to
      # @current_user = @current_user || User.find_by(id: session[:user_id])
   end
   
   def logged_in?
      !current_user.nil?
      # Return true if user is logged in, false otherwise
   end
   #              [ 8.2.3:15 -> _header.html.erb] 
   # A user is logged in if there is a current user in the session, 
   # IE current_user is not nil

   
   # Forgets a persisitent session
   def forget(user)
      user.forget
      cookies.delete(:user_id)
      cookies.delete(:remember_token)
   end
   # [ 8.4.3:39 -> user_login_test.rb ] Logging out from a persisitensession


   def log_out
      forget(current_user)
      session.delete(:user_id)
      @current_user = nil
   end
   # [ 8.3:26 -> sessions_controller.rb ] log-out method
   # Logging out involves undoing the effects of the log_in method, which involves
   # deleting the user id from the session

  # Redirects to stored location (or to the default).
  def redirect_back_or(default)
    redirect_to(session[:forwarding_url] || default)
    session.delete(:forwarding_url)
  end
  
  # Stores the URL trying to be accessed.
  def store_location
     # Puts requested URL in the session variable under the key :forwarding_url, 
     # but only for a get request.
     # This prevents storing the forwarding URL if a user, say, submits a form
     # when not logged on
    session[:forwarding_url] = request.url if request.get?
  end
   # [ 9.2.3:27 -> users_controller.rb ] Code to implement friendly forwarding
   # n order to forward users to their intended destination, we need to store 
   # the location of the requested page somewhere,and then redirect to that 
   # location instead of the default. we accomplish this with a pair of methods,
   # store_location and redirect_back_or

end



   #             [ 8.2.1:12 ->  sessions_controller.rb ]
   # treat session as a hash.
   # This places a temp cookie on user's browser containing encrypted v. of user id
   # which we can retrieve by using session[:user_id]
   # cookie expires when browser closes
   #
   # we're defining log_in in a helper because it'll be used in a couple of dif
   # places