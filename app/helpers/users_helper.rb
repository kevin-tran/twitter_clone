module UsersHelper
   
  
  def gravatar_for(user, options = { size: 80 })
    gravatar_id = Digest::MD5::hexdigest(user.email.downcase)
    size = options[:size]
    gravatar_url = "https://secure.gravatar.com/avatar/#{gravatar_id}?s=#{size}"
    image_tag(gravatar_url, alt: user.name, class: "gravatar")
  end
  #**                [ 7.1.4:9 -> show.html.erb ]
  #** Returns the Gravatar for the given user.
  #** It also returns an image tag for the Gravatar with a gravatar CSS class
  #** and alt text equal to the user's name (conveneint for the sight-impaired 
  #** users using a screen reader).
  
end
