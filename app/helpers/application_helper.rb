module ApplicationHelper
   
   #** Creating a title function
   #** Returns the full title on a per-page basis
   #** All functions in here are automagically included in
   #** application.html.erb/views
   
   def full_title(page_title ="")
      base_title = "Ruby on Rails Twitter Clone"
      if page_title.empty?
         base_title
      else
         page_title + " | " + base_title
         # "#{page_title} | #{base_title}"
      end
   end
end
