class UserMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.user_mailer.account_activation.subject
  #
  def account_activation(user)
    @user = user
    mail to: user.email, subject: "Account Activation"
  end
  # [ 10.1.2:11 -> ] Mailing the account activation link
  # create instance variable containing the user (for use in the view) and then
  # mail the result to user.email
  

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.user_mailer.password_reset.subject
  #
  def password_reset(user)
    @user = user
    mail to: user.email, subject: "Password Reset"
  end
  # [ 10.2.3:43 -> password_reset.html.rb ] Mailing the password reset link
  
end
