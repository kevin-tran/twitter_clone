class StaticPagesController < ApplicationController
  
  # actions are empty so the controller moves to the view
  def home
    if logged_in? 
     @micropost = current_user.microposts.build
    # [ 11.3.1:38 -> _error_messages.html.erb ] 
    # adding micropost inst var to home action
      @feed_items = current_user.feed.paginate(page: params[:page])
    # [ 11.3.3:45 -> _feed.html.erb] add feed instance var
    end

  end

  def help
  end
  
  def about
  end
  
  def contact
  end
end

# [ 3.2.1:4 -> routes.rb ] Creating Static Pages controller
# Rails actions become bundled together inside controllers,
# which contains sets of actions related by purpose.