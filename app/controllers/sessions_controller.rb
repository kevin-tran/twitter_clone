class SessionsController < ApplicationController
  def new
    #** since no body to action, controller requests the view which is merely
    #** the login page
  end
  
  def create
    user = User.find_by(email: params[:session][:email].downcase)
    #** find_by("email" => params[:session][:email].downcase)
    if user && user.authenticate(params[:session][:password])
      #** Log the user in and redirect to the User's show page
      if user.activated? 
        log_in user
        params[:session][:remember_me] == '1' ? remember(user) : forget(user)
        # value of params[:session][:remember_me] is '1' if ticked and '0' if not
        # [ 8.4.5: 49 -> test_helper.rb ] Handling submission of the  'remember me' checkbox
         redirect_back_or user
        # To implement the forwarding itself, we use the redirect_back_or method
        # to redirect to the requested URL if it exists, or some default URL otherwise
      else
        message = "Account not activated. "
        message += "Check your email for the activation link."
        flash[:warning] = message
        redirect_to root_url
      end
    else
      flash.now[:danger] = "Invalid email/password combination"
      render 'new'
    end
  end
  
  def destroy
    log_out if logged_in?
    redirect_to root_url
  end
  # [ 8.3:26 -> user_login_test.rb ] Destroying a session
  # [ 8.4.4:42 -> user_test.rb ] Only log out if logged in
  # [ 10.1.2:30 -> users_signup_test.rb ] preventing unactivated users from logging in
end

   #               [ 8.1.3:4 -> stay ]
  # create action does nothing but render the new view.
  
  #                [ 8.1.3:5 & 6-> user_login_test.rb ]
  # ~ The POST request to the create action supplies us with a nested params hash
  # session: { password: < .. >, email: < ..> } 
  # thus params[:session][:email] gives us the submitted email
  # and params[:session][:password] gives us the password
  # ~ IN OTHER WORDS, the params hash inside the create action has all the info
  # we need to authenticate users.
  # ~ the if statement is true only if a user with the given email both exists
  # in the DB and has the given password
  
  # ~ Since sessions isn't an active record model, we can't use the User model error
  # messages. We'll simply flsah that betch. However, it's not quite finished.
  # rerendering a template with render doesn't count as a request and so the flash 
  # will stay until we make a request.
  # ~ To resolve, we'll
  
  #               [ 8.2.1:13 -> sessions_helper.rb ] Loggin in a user
  # "redirect_to user" is equivalent to "redirect_to user_id(user)"
  # No way of showing that user is logged in just yet.
  #
  #
  
  #               [ 8.4.2:34 -> sessions_helper.rb ] Logging in and remembering a user
  # Defer work to Sessions helper, where we define a remember method that 
  # calls user.remember, thereby generating a remember token and saving its 
  # digest to the DB.
  # It then uses cookies to create permanent cookies for the user id and remember token
  
  # [ 9.2.3:29 SKIP30 -> users_controller_test.rb ] The sessions create action with friendly forwarding