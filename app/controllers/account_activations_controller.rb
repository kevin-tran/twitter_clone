class AccountActivationsController < ApplicationController
   
   def edit
      user  = User.find_by(email: params[:email])
      if user && !user.activated? && user.authenticated?(:activation, params[:id])
         # 1) User exists in database
         # 2) User hasn't been activated already
         # 3) authenticate
         user.activate
         #** [ 10.1.4 -> routes.rb ] Acct activation via user model object
         log_in user
         flash[:success] = "Account activated!"
         redirect_to user
      else
         flash[:danger] = "Invalid activation link"
         redirect_to root_url
      end
   end
end

# [ 10.1.2:29 -> sesions_controller.rb ] edit action to activate accounts