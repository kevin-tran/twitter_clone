class UsersController < ApplicationController
  before_action :logged_in_user,  only: [:index, :edit, :update, :destroy,
                                         :following, :followers]
  #** Require's a user to be logged in then invoke it by using before_action
  #** By default, before filters apply to every action in a controller, so here 
  #** we restrict the filter to act only on the :edit and :update actions 
  #** by passing the appropriate :only options hash.
  
  
  
  before_action :correct_user,    only: [:edit, :update]
  #** [ 9.2.2:22 (SKIP 23) -> sessions_helper.rb ] A correct_user before filter to protect the edit/update pages
  
  
  before_action :admin_user,      only: :destroy
  # Enforce access control by using a before filter, this time to restrict
  # access to the destroy action only to admins
  
  
  
  def index
    @users = User.paginate(page: params[:page])
    # paginate method will paginate the users int he app by using paginate in
    # place of all in the index action. the page paramter comes from params[:page]
    # which is generated automaticalyl by will_paginate
  end
  # [ 9.3.1:32 -> STAY ] Requiring a logged-in user for index action
      # include it in list of actions protected by logged_in_user before filter
  # [ 9.3.1:33 -> index.html.erb ] User index action
  # [ 9.3.3:42 > users.yml
  
  
  def show 
    @user = User.find(params[:id])
    @microposts = @user.microposts.paginate(page: params[:page])
    #** [ 11.2.1:22 -> view/users/show.html.erb ]
    #** adding @micropost instance variable to users show act
    #** will_paginate assumes the existence of a variable called @users, but in 
    #** in this case where we're still in Users controller but want to paginate
    #** micropsots instead, we'll pass an explicit @microposts variable to paginate
  end
  #    GET       [ 7.1.2:5 -> show.html.erb, skip 6 & 7]
  # Define a user variable
  # When we make the request(by going to URL/users/1) to the Users controller,
  # params[:id] will be the user id 1.
  
  
  
  def new
    @user = User.new
  end
  #             [ 7.2.1:12 -> new.html.erb ]
  # The signup page is routed to the new action, our first step is to create the
  # User object that needs to be passed as an argument to the Rails fxn, form_for
  
  
  
  def create
    @user = User.new(user_params)
    if @user.save
      @user.send_activation_email
      #** [ 10.1.4:34 -> account_activations_controller.rb ]
      #** sending email via user model object
      flash[:info] = "Please check your email to activate your account."
      redirect_to root_url
      #** [ 10.1.2:21 SKIP 22) -> user.rb ] adding acct activation to user signup
      #**since we're requiring accoutn activation, we redirect to root_url
      #** rather than the user's profile page.
      # redirect_to @user
      # ALT it can be redirect_to user_url(@user)
    else
      render 'new'
    end
  end   
    #             [ 7.3.1:16 -> stay ]
    # ~render also works in a controller. we use an if-else branching to handle 
    # the cases of success and failure based on the value of @user.save (T/F)
    # @user = User.new(params[:user]) is equivalent to 
    # @user = User.new(name: "Foo Bar", email: "foo@invalid",
    #             password: "foo", password_confirmation: "bar")
    #
    # ~ @user = User.new(params[:user]) is dangerous. we could allow any user to  
    # to gain administrive access by including admin='1' in the web request.
    # to remedy this, we create a helper function to restrict permissions
    # ~ The form will fail on a valid submission (at this point) because there is
    # no corresponding view for this create action. 
    
    #             [ 7.4.1:23 -> stay ]
    # rather than render a page on a successful user creation, we'll redirect to
    # a different page, the newly created user's profile.
    
    #             [ 7.4.1:24 -> application.html.erb]
    # flash is method which we treat as a hash. the :success key results in a 
    # successful message. 
    # Each message, hash (IE succes: "welcome..."), will be inserted and then
    # removed once it has displayed. Thus allowing for new messages or a template
    # for a message.
    
    #           [ 8.2.5:22 -> test/test_helper.rb ] Logging in the user upon signup
    # Since we included sessions_helper module in application controller, it is 
    # available in all controllers.
    
    
    
    def edit
      @user = User.find(params[:id])
    end
    #             [ 9.1.1:1 -> edit.html.erb ] 
    # The User edit action
    
    
    
    def update
      @user = User.find(params[:id])
      if @user.update_attributes(user_params)
        # Handle a successful update.
        flash[:success] = "Profile updated"
        redirect_to @user
      else
        render 'edit'
      end
    end    
    # [ 9.1.2:5 -> users_edit_test.rb ] Initial user update action
    # [ 9.1.4:9 -> User.rb] User update action
      # to flash success and redirect to profile  
    
    
    def destroy 
      User.find(params[:id]).destroy
      #** Destroyed with the ActiveRecord destroy method.
      flash[:success] = "User deleted"
      redirect_to users_url
      # to list of users
    end    
    # [ 9.4.2:53 -> STAY ] Add working destroy action
    
    
  def following
    @title = "Following"
    @user  = User.find(params[:id])
    @users = @user.following.paginate(page: params[:page])
    render 'show_follow'
  end

  def followers
    @title = "Followers"
    @user  = User.find(params[:id])
    @users = @user.followers.paginate(page: params[:page])
    render 'show_follow'
  end
  # [ 12.2.3:25 -> show_follow.html.erb] 
  # The following and followers action, created a before_action as well.
  # By rails Convention, it implicitly renders the template corresponding to the
  # an action, such as rendering show.html.erb at the end of the show action.
  # In this case, both actions make an explicit call to render, to render a view
  # called show_follow.
    
    
    
    private
    
      def user_params
        params.require(:user).permit(:name, :email, :password,
                                     :password_confirmation)
      end
      #             [ 7.3.2:17 -> new.html.erb ]
      # ~we want to require the params hash to have have a :user attribute, and we
      # want to permit the name, email, password, and password_confirmation (but no
      # others). 
      # ~ This code returns a version of the params hash with only permitted attr
      # ~ Since this code is only used internally by the Users controller and
      # need not be exposed to external users via the web, we make it private.

    # Before filters

    # # Confirms a logged-in user.
    # def logged_in_user
    #   unless logged_in?
    #     store_location
    #     flash[:danger] = "Please log in."
    #     redirect_to login_url
    #   end
    # end
    # [ 9.2.1:12 (SKIP 13) -> users_edit_test.rb ] Adding a logged_in_user before filter 
    # [ 9.2.3:28 -> sessions_controller.rb ] Adding store_location to the loggedd-
    # in user before filter



   # Confirms the correct user.
    def correct_user
      @user = User.find(params[:id])
      redirect_to(root_url) unless current_user?(@user)
    end
    #** [ 9.2.2:22 skip23 -> sessions_helper.rb ] A correct_user before filter
    #** to protect the edit/update pages
    #** [ 9.2.2:25 -> users_test.rb]  finall correct_user before filter

    # Confirms an admin user.
    def admin_user
      redirect_to(root_url) unless current_user.admin?
    end

end
