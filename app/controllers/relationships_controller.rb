class RelationshipsController < ApplicationController
   before_action :logged_in_user 
   # [ 12.2.4:31,32-> _follow.html.erb   ]
   # Access control for relationships
   
  # Actions to create follow/unfollow using Ajax
 
  def create
    @user = User.find(params[:followed_id])
    current_user.follow(@user)
    respond_to do |format|
      format.html { redirect_to @user }
      format.js
    end
  end

  def destroy
    @user = Relationship.find(params[:id]).followed
    current_user.unfollow(@user)
    respond_to do |format|
      format.html { redirect_to @user }
      format.js
    end
  end
  # [ 12.2.5:35 -> application.rb ]
  # Responding to Ajax requests in the Relationships controller.
  # 1) respond_to is more like an if-then-else statement.
  # 2) Adapting relationships controller to respond to Ajax involves adding
      # respond_to.
   
   
   
   
   
   
  # Actions for creating follow/unfollow button the standard way 
  #
  # def create
  #   user = User.find(params[:followed_id])
  #   current_user.follow(user)
  #   redirect_to user
  # end
  # # Find the user associated with the followed_id in the corresponding form, 
  # # _follow.html.erb, 
  

  # def destroy
  #   user = Relationship.find(params[:id]).followed
  #   current_user.unfollow(user)
  #   redirect_to user
  # end
  
  
end

