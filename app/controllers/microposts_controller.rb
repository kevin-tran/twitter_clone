class MicropostsController < ApplicationController

  before_action :logged_in_user, only: [:create, :destroy]
   # restrict access to add & destroy actions thru before filter

   before_action :correct_user, only: :destroy

  def create
     # diff from user create because of the use of the user/microposts assoct
     # to build the new micrpost
    @micropost = current_user.microposts.build(micropost_params)
    if @micropost.save
      flash[:success] = "Micropost created!"
      redirect_to root_url
    else
      @feed_items = []
      render 'static_pages/home'
    end
  end

  def destroy
     @micropost.destroy
     #** Destroyed with the ActiveRecord destroy method.
    flash[:success] = "Micropost deleted"
    redirect_to request.referrer || root_url    
    # referrer method is just the previous URL, in this case, the Home page
    # [ 11.3.4:50 -> microposts.yml ] Microposts controller destroy action
  end
  
  
   private

    def micropost_params
      params.require(:micropost).permit(:content, :picture)
      # [ 11:58 -> _micropost.html.erb ]
      # adding picture to list of permitted attributes
    end
   
   def correct_user
      @micropost = current_user.microposts.find_by(id: params[:id])
      redirect_to root_url if @micropost.nil?
   end
end
# [ 11.3.1:32 skip 33-> STAY ]  adding authorization
# [ 11.3.1:34 -> static_pages/home.html.erb] microposts controller create action