class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  
  include SessionsHelper
  
  private

    # Confirms a logged-in user.
    def logged_in_user
      unless logged_in?
        store_location
        flash[:danger] = "Please log in."
        redirect_to login_url
      end
    end  
  # [ 11.3.1:31 -> ] Moving logged_in_user method to app controller
  # method will be available in base class of all controllers, so it can be used
  # in micrposts controller
  
  
end


  #             [ 8.1.5:11 -> helpers/sessions_helper.rb ]
  # A sesssions helper module was created when we generated the sessions controller.
  # By including it here, we wil arrange to make it available in all our controllers