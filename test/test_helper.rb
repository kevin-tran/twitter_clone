ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'

# to add da colors nahmsayin??
require "minitest/reporters"
Minitest::Reporters.use!

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all

  # Add more helper methods to be used by all tests here...
  def is_logged_in?
    !session[:user_id].nil?
    # Return true if a test user is logged in
  end
  #             [ 8.2.5:23 -> users_signup_test.rb] Boolean method for login status inside tests
  # different from sessions_helper method logged_in because that counts on current
  # user. Helper methods aren't available in tests, but session method is 
  
  # Lgs in a test user.
  def log_in_as(user, options = {})
    password    = options[:password]    || 'password'
    # Will evaluate to the given option if present and to the dafult otherwise
    remember_me = options[:remember_me] || '1'
    if integration_test?
      # Log in by posting ot the sessions path
      post login_path, session: { email:        user.email,
                                  password:     password,
                                  remember_me:  remember_me }
    else
      # Log in using the session
      session[:user_id] = user.id
    end
    
  end
  
    private
    
    #Returns true inside an integration test
    def integration_test?
      defined?(post_via_redirect)
      # post_via_redirect is availble only in integration tests
    end
    # [ 8.4.5:51 -> user_login_test.rb ] Adding Log_in_as helper

end
