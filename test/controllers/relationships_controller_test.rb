require 'test_helper'

class RelationshipsControllerTest < ActionController::TestCase

  test "create should require logged-in user" do
    assert_no_difference 'Relationship.count' do
      post :create
    end
    assert_redirected_to login_url
  end

  test "destroy should require logged-in user" do
    assert_no_difference 'Relationship.count' do
      delete :destroy, id: relationships(:one)
    end
    assert_redirected_to login_url
  end
  # [ 12.2.4:30 -> relationships_controller.rb  ] 
  # Basic access control tests for relationships
  #  require logged in user while also not changing the relationship cout
  # will fail until adding logged_in_user before filter
  


end
