require 'test_helper'

class UsersControllerTest < ActionController::TestCase
  def setup
    @user       = users(:michael)
    @other_user = users(:archer)
  end  
  
  test "should redirect index when not logged in" do
    get :index
    assert_redirected_to login_url
  end
  # [ 9.3.1:31 -> users_controller.rb ] Testing index action redirect
  # To protect the index page from unauthorized access, we'll add test to verify
  # that the index action is redirected properly
  
  test "should get new" do
    get :new
    assert_response :success
    assert_select "title", "Sign Up | Ruby on Rails Twitter Clone"
  end


 
  test "should redirect edit when not logged in" do
    get :edit, id: @user
    assert_not flash.empty?
    assert_redirected_to login_url
  end

  test "should redirect update when not logged in" do
    patch :update, id: @user, user: { name: @user.name, email: @user.email }
    assert_not flash.empty?
    assert_redirected_to login_url
  end
# [ 9.2.1:17 -> users.yml ] Testing that edit and update are protected
# The plan is to hit the edit and update action with the right kinds of 
# requests and verify that the flash is set and that the user is
# redirected to the login path.
# This will make remove security flaw of updating when not logged in.
  
  
  test "should redirect edit when logged in as wrong user" do
    log_in_as(@other_user)
    get :edit, id: @user
    assert flash.empty?
    assert_redirected_to root_url
    # Expect to redirect users to the root path instead of the login path
    # because a user trying to edit a different user would already be logged in
  end

  test "should redirect update when logged in as wrong user" do
    log_in_as(@other_user)
    patch :update, id: @user, user: { name: @user.name, email: @user.email }
    assert flash.empty?
    assert_redirected_to root_url
  end
  # [ 9.2.2:21 -> users_controller.rb ] Tests for trying to edit as the wrong user.
  
  test "should redirect destroy when not logged in" do
    assert_no_difference 'User.count' do
      delete :destroy, id: @user
    end
    assert_redirected_to login_url
  end

  test "should redirect destroy when logged in as a non-admin" do
    log_in_as(@other_user)
    assert_no_difference 'User.count' do
      delete :destroy, id: @user
    end
    assert_redirected_to root_url
  end
  # Use delete to issue a DELETE request directly tot he destroy action
  # check two cases:
  # 1) users who aren't logged in should be redirected to login page
  # 2) users who are logged inbut aren't admins should be redirected to the home pg
  
  
  test "should redirect following when not logged in" do
    get :following, id: @user
    assert_redirected_to login_url
  end

  test "should redirect followers when not logged in" do
    get :followers, id: @user
    assert_redirected_to login_url
  end
  # [ 12.2.3:24 -> users_controller.rb ] 
  # Tests for authoriziation of of the following and followers page
  
  
end


# [ 9.4.2:56 -> users_index_test.rb ] action level tests for admin access control