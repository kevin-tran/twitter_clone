require 'test_helper'

class UsersSignupTest < ActionDispatch::IntegrationTest

  def setup
    ActionMailer::Base.deliveries.clear
  end
  
  
  test "invalid signup information" do
    get signup_path
    assert_no_difference 'User.count' do
      post users_path, user: { name:  "",
                               email: "user@invalid",
                               password:              "foo",
                               password_confirmation: "bar" }
    end
    assert_template 'users/new'
    assert_select 'div#error_explanation'
    assert_select 'div.field_with_errors'
  end

  test "valid signup information with account activation" do
    get signup_path
    assert_difference 'User.count', 1 do
      post users_path, user: { name:  "Example User",
                               email: "user@example.com",
                               password:              "password",
                               password_confirmation: "password" }
    end
    assert_equal 1, ActionMailer::Base.deliveries.size
    user = assigns(:user)
    assert_not user.activated?
    # Try to log in before activation.
    log_in_as(user)
    assert_not is_logged_in?
    # Invalid activation token
    get edit_account_activation_path("invalid token")
    assert_not is_logged_in?
    # Valid token, wrong email
    get edit_account_activation_path(user.activation_token, email: 'wrong')
    assert_not is_logged_in?
    # Valid activation token
    get edit_account_activation_path(user.activation_token, email: user.email)
    assert user.reload.activated?
    follow_redirect!
    assert_template 'users/show'
    assert is_logged_in?
  end
end



  #               [ 7.3.4:21 -> users_controller.rb ]
  # 1) Start by visitng the signup_path by using GET
  # 2) check there is no difference to User.count (updates in DB). 
  # 3) Run block
  #     1) issue a POST request to the users_path
  #     2) we include the params[:user] hash expected by User.new in the create 
  #       action.
  # 4) Check to see if count has changed
  # 5) Ensure that a failed submission re-renders the new action
  
  
  #               [ 7.4.4:26 -> routes.rb ]
  # changed the test from assert_no_difference to assert_difference regarding
  # the first string argument, 'User.count', which arranges for a comparison 
  # before and after the the contents of the block. 
  # the 2nd arg specifies the size of the difference
  # post_via_redirect arranges to follow the redirect after submission,
  # resulting in a rendering of the 'users/show' template

  #               [ 8.2.4:24 (SKIP 25) -> sessions_helper.rb ] 
  # add test for login after signup