require 'test_helper'

class UsersEditTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end
  
  def setup
    @user = users(:michael)
  end

  test "unsuccessful edit" do
    log_in_as(@user)
    get edit_user_path(@user)
    assert_template 'users/edit'
    patch user_path(@user), user: { name:  "",
                                    email: "foo@invalid",
                                    password:              "foo",
                                    password_confirmation: "bar" }
    assert_template 'users/edit'
  end
  
#         [ 9.1.3:6 (sKIP 7 -> STAY ] testfor an unsuccessful edit
# checks for correct behavor by verifying that the edit template is
# rendered after getting the edit page and re-rendered upon submission of invalid 
# information.
# Patch method issues a Patch request
  
  test "successful edit with friendly forwarding" do
    get edit_user_path(@user)
    log_in_as(@user)
    assert_redirected_to edit_user_path(@user)
    name  = "Foo Bar"
    email = "foo@bar.com"
    patch user_path(@user), user: { name:  name,
                                    email: email,
                                    password:              "",
                                    password_confirmation: "" }
    assert_not flash.empty?
    assert_redirected_to @user
    @user.reload
    assert_equal @user.name,  name
    assert_equal @user.email, email

  end
  # [ 9.1.4:8 -> users_controller.erb ] Test of successful edit
    # password and confirmation are blank, for users who don't want t update their
    # passwords everytime they update their names or email
  # [ 9.2.1:14 (skip 15 & 16) -> users_controller_test.rb ] Loggin in a test user 
  # [ 9.2.3:26 -> sesions_helper.rb  ] test for friendly forwarding
    # test will try to visit the edit page, then log in, and then checks 
    # that the user is redirected to the edit apge instead of the default
    # profile page
end

