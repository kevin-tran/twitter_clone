require 'test_helper'

class SiteLayoutTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end
  
  test "layout links" do
    get root_path
    assert_template 'static_pages/home'
    #** a_t verifies that the Home page is rendered using the correct view
    assert_select "a[href=?]", root_path, count: 2
    #** assert_select searches for tag <a> with attri "href" and then inserts
    #** the root_path into the '?'
    assert_select "a[href=?]", help_path
    assert_select "a[href=?]", about_path
    assert_select "a[href=?]", contact_path  
  end
  
  
end

#** [ 5.3.4:25 (SKIP 26-32) -> routes.rb ] test for links on layout 
#** 1) Get the root path (Home page)
#** 2) Verify that the right page template is rendered
#** 3) Check for the correct links to the Home, Help, About, and Contact pages