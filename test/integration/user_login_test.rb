require 'test_helper'

class UserLoginTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end
  
  def setup
    @user = users(:michael)
    # References the Fixture user in users.yml
  end

  
  
  test "login with invalid information" do
    get login_path
    assert_template "sessions/new"
    post login_path, session: { email: "", password: ""}
    assert_template 'sessions/new'
    assert_not flash.empty?
    get root_path
    assert flash.empty?
  end
  #                [ 8.1.5: 7 skip 8,9,10 -> application_controller.rb ]
  # Go to login path, check that the view is for the login
  # submit a POST request with an invalid params hash
  # confirm that we are redirected to the login
  # confirm that a error message is flashed
  # make a request for root page
  # confirm that flash message dissappears


  test "login with valid information followed by a logout" do
    get login_path
    post login_path, session: { email: @user.email, password: 'password' }
    assert is_logged_in?
    assert_redirected_to @user # To check the right redirect target
    follow_redirect! # to actually visit the target page
    assert_template 'users/show'
    assert_select "a[href=?]", login_path, count: 0 # verify login link disappears
    assert_select "a[href=?]", logout_path
    assert_select "a[href=?]", user_path(@user)
    delete logout_path
    assert_not is_logged_in?
    assert_redirected_to root_url
    # Simulate a user clicking logout in a second window.
    delete logout_path # should raise an error due to missing current_user
    follow_redirect!
    assert_select "a[href=?]", login_path
    assert_select "a[href=?]", logout_path,      count: 0
    assert_select "a[href=?]", user_path(@user), count: 0
  end
  # [ 8.2.4:20 (skip 21) -> users_controller.rb ] Add test for user logging in with valid information
  
  # [ 8.3:28  (SKIP 29) -> [timestamp]_add_remember_digest_to_users.rb ] Added test for user logout
  # Use delete to issue a DELETE request to the logout path and verify that the user is logged out
  # and redirected to the root URL

  # [ 8.4.4:40 (SKIP 41) -> sessions_controller.rb ] Test for user logout
  
  test "login with remembering" do
    log_in_as(@user, remember_me: '1')
    assert_not_nil cookies['remember_token']
    # cookies methods doesn't work with symbols, but will work with strings
  end
  
  test "login without remembering" do
    log_in_as(@user, remember_me: '0')
    assert_nil cookies['remember_token']
  end
  
  # [8.4.5:51  (SKIP 52) -> sessions_helper.rb ] Test of the 'remember me' checkbox
  
end
