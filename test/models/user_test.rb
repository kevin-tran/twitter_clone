require 'test_helper'

class UserTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  
  
  def setup
    @user = User.new(name: "Example User", email: "user@example.com",
                     password: "foobar", password_confirmation: "foobar")
    #** ~ has_secure_password enforces validations on the virtual password &
    #** pass_conf attributes
  end
  
  test "should be valid" do 
    assert @user.valid?
  end
  
  
  
  test "name should be present" do
    @user.name = "    "
    assert_not @user.valid?
    #** assert_not means we should get false
  end
  
  test "email should be present" do
    @user.email = " "
    assert_not @user.valid?
  end
  #** the effects of the validation method we added to our User model class
  #** will not allow empty strings to be accepted, thus resulting in False
  #** and these tests passing.
  
  
  test "name should not be too long" do
    @user.name = 'a' * 51
    assert_not @user.valid?
  end
  
  test "email should not be to long" do
    @user.email = 'a' * 244 + "@example.com"
    assert_not @user.valid?
  end
  
  test "email validation should accept valid addresses" do
    valid_addresses = %w[user@example.com USER@foo.COM A_US-ER@foo.bar.org
                         first.last@foo.jp alice+bob@baz.cn]
    valid_addresses.each do |valid_address|
      @user.email = valid_address
      assert @user.valid?, "#{valid_address.inspect} should be valid"
      #** we've entered an optional argument for the assertion to supply a 
      #** custom error which identifies the address causing the test to fail
    end
  end
  
    test "email validation should reject invalid addresses" do
    invalid_addresses = %w[user@example,com user_at_foo.org user.name@example.
                           foo@bar_baz.com foo@bar+baz.com]
    invalid_addresses.each do |invalid_address|
      @user.email = invalid_address
      assert_not @user.valid?, "#{invalid_address.inspect} should be invalid"
    end
  end
  
  test "email addresses should be unique" do
    duplicate_user = @user.dup
    duplicate_user.email = @user.email.upcase
    @user.save
    assert_not duplicate_user.valid?
  end
  #**make a user with teh same email as @user using @user.dup, which creates a
  #** duplicate user with the same attributes
  #** since we save @user, the duplicate user has an email already in the DB and thus
  #** should not be valid. 
  #** since there is no initial restraint, this would be valid and thus fail.
  
  test "password should have a minimum length" do
    @user.password = @user.password_confirmation = "a" * 5
    assert_not @user.valid?
  end
  #** ~ use of COMPACT MULTIPLE ASSIGNMENT assigns pass and pass_conf to have
  #**  the same stored value no initial restrictions, so it'll fail.
  
  test "authenticated? should return false for a user with nil digest" do
    assert_not @user.authenticated?(:remember, '')
  end
  # [ 8.4.4:43 SKIP 44 -> user.rb ] test of authenticated with nonexistent digest
  # [10.1.2:27 -> account_activations_controller.rb ] using generalized authenticated method
  
  test "associated microposts should be destroyed" do
    @user.save
    @user.microposts.create!(content: "Lorem ipsum")
    assert_difference 'Micropost.count', -1 do
      @user.destroy
    end
  end
  # [ 11.1.4:19 skip 20-> _micropost.html.erb ] test of dependent: :destroy
  
  
  test "should follow and unfollow a user" do
    michael = users(:michael)
    archer  = users(:archer)
    assert_not michael.following?(archer)
    michael.follow(archer)
    assert michael.following?(archer)
    assert archer.followers.include?(michael)    
    michael.unfollow(archer)
    assert_not michael.following?(archer)
  end
  # [ 12.1.4:9 -> models/user.rb ] 
  # test for some "following" utility methods
  # should fail initially because no follow, unfollow, and following? methods
  
  
  test "feed should have the right posts" do
    michael = users(:michael)
    archer  = users(:archer)
    lana    = users(:lana)
    # Posts from followed user
    lana.microposts.each do |post_following|
      assert michael.feed.include?(post_following)
    end
    # Posts from self
    michael.microposts.each do |post_self|
      assert michael.feed.include?(post_self)
    end
    # Posts from unfollowed user
    archer.microposts.each do |post_unfollowed|
      assert_not michael.feed.include?(post_unfollowed)
    end
  end
  # [12.3.1:41 -> ] 
  # Test for status feed
  
end
