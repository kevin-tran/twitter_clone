require 'test_helper'

class MicropostTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end

  def setup
    @user = users(:michael)
    # This code is not idiomatically correct.
    @micropost = @user.microposts.build(content: "Lorem ipsum")
    #** [ 11.1.3:11 skip 12 -> stay ]
    #** build returns an object in memory that doesn't modify the DB. also, it
    #** automatically associates micropost with user
  end

  test "should be valid" do
    assert @micropost.valid?
  end

  test "user id should be present" do
    @micropost.user_id = nil
    assert_not @micropost.valid?
    # will fail at first b/c there no initial validations on micropost model
  end
  
  # [ 11.1.2:2 -> ] Test for validity of new micropost
  # 1) in setup, we create new micropost while associating it with valid user
  # fromt he fixture
  # 2) check that the result is valid
  # 3) b/c every micropost should have a user id, we'll test for user_id presence validation

  test "content should be present " do
    @micropost.content = "   "
    assert_not @micropost.valid?
  end

  test "content should be at most 140 characters" do
    @micropost.content = "a" * 141
    assert_not @micropost.valid?
  end
  # [ 11.12:6 skip 7,8 -> ] Test for micropost model validations
  
   test "order should be most recent first" do
    assert_equal Micropost.first, microposts(:most_recent)
  end
  # [ 11.1.4:13 -> microposts.yml ] testing micropost order
  
end
