require 'test_helper'

class SessionsHelperTest < ActionView::TestCase

  def setup
    @user = users(:michael)
    remember(@user)
  end

  test "current_user returns right user when session is nil" do
    assert_equal @user, current_user
    assert is_logged_in?
  end

  test "current_user returns nil when remember digest is wrong" do
    @user.update_attribute(:remember_digest, User.digest(User.new_token))
    assert_nil current_user
  end
end

# [ 8.4.5:55 -> sessions_helper.rb ] A test for persistent sessions
# Because the remember method doesn't set session[:user_id], this procedure will
# test the desired "remember" branch
# Test sequence:
# 1) Define a user variable using the fixtures.
# 2) Call the remember method to remember the given user.
# 3)Verify that current_user is equal to the given user.
# 
# Will check that the current user is nil if the user's remember digest doesn't
# correspond correctly to the remember token, thereby testing the authenticated?
# expression in the nested if statement'
# if user && user.authenticated?(cookies[:remember_token])