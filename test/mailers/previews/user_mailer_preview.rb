# Preview all emails at http://localhost:3000/rails/mailers/user_mailer
class UserMailerPreview < ActionMailer::Preview

  # Preview this email at rails-tutorial-kevintran.c9.io/rails/mailers/user_mailer/account_activation
  def account_activation
    user = User.first
    user.activation_token = User.new_token
    # Important because acct activation templates require an acct activation token
    UserMailer.account_activation(user)
  end
  # [ 10.1.2:16 -> user_mailer_test.rb ] working preview method for account activation
  

  # Preview this email at rails-tutorial-kevintran.c9.io/rails/mailers/user_mailer/password_rest
  def password_reset
    user = User.first
    user.reset_token = User.new_token

    UserMailer.password_reset
  end

end
# [ 10.2.3:46 -> user_mailer_test.rb ] Working preview method for password reset