require 'test_helper'

class UserMailerTest < ActionMailer::TestCase
  test "account_activation" do
    user = users(:michael)
    user.activation_token = User.new_token
    mail = UserMailer.account_activation(user)
    assert_equal "Account Activation", mail.subject
    assert_equal [user.email], mail.to
    assert_equal ["noreply@example.com"], mail.from
    assert_match user.name,               mail.body.encoded
    assert_match user.activation_token,   mail.body.encoded
    assert_match CGI::escape(user.email), mail.body.encoded
      #** used to escape the test user's email.
    #** Uses assert_match to check that the name, activation token, and 
    #** escaped email appear int he email's body
  end
  #** [10.1.2:17 (SKIP 18) -> environment/test.rb ] test of current email implementation
  
  
  test "password_reset" do 
    user = users(:michael)
    user.reset_token = User.new_token
    mail = UserMailer.password_reset(user)
    assert_equal "Password Reset", mail.subject
    assert_equal [user.email], mail.to
    assert_equal ["noreply@example.com"], mail.from
    assert_match user.reset_token,        mail.body.encoded
    assert_match CGI::escape(user.email), mail.body.encoded
  end    
  # [ 10.2.3:47 SKIP 48,49 -> views/password_resets/edit.html.erb ] 
  # Adding a test of the password reset mailer method
  # unlike activation token, which is created for ever user by the before_create
  # rollback, password reset token is craeted only when a user successfully submites
  # the "forgot password" form
  
  
end



# assert_match 'foo', 'foobar'      # true
# assert_match 'baz', 'foobar'      # false
# assert_match /\w+/, 'foobar'      # true
# assert_match /\w+/, '$#!*+@'      # false