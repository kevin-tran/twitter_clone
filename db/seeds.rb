# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


User.create!(name:  "Example User",
             email: "example@railstutorial.org",
             password:              "foobar",
             password_confirmation: "foobar",
             admin: true,
             activated: true,
             activated_at: Time.zone.now)

99.times do |n|
  name  = Faker::Name.name
  email = "example-#{n+1}@railstutorial.org"
  password = "password"
  User.create!(name:  name,
               email: email,
               password:              password,
               password_confirmation: password,
              activated: true,
              activated_at: Time.zone.now)               
end

# [ 9.3.2:39 (SKIP 40) -> index.html.erb ] A rake task for seeding the database with sample users
# This creates an example user with name and email address replicating our
# previous one, and then makes 99 more
#~ The create! method is just like the create method, except it raises an exception
# for an invalid user rather than returning false. This behavior makes 
# debugging easier by avoiding silent errors.

# [ 9.4.1:51 -> _user.html.erb ]seed data code with an admin user 

# MICROPOSTS

users = User.order(:created_at).take(6)
# 1) to use the first 5 users with custom gravatars and one with defautl gravatar
# 2) order is to ensure first 6 that were created

50.times do
  content = Faker::Lorem.sentence(5)
  users.each { |user| user.microposts.create!(content: content) }
end
# [ 11.2.2:24 -> custom.css.scss ] adding microposts to sample data



# Following relationships
users = User.all
user  = users.first
following = users[2..50]
followers = users[3..40]
following.each { |followed| user.follow(followed) }
followers.each { |follower| follower.follow(user) }

# [ 12.2.1:14 -> routes.rb ] 
# Adding following/follower relationships to sample data
