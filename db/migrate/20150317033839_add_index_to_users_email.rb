class AddIndexToUsersEmail < ActiveRecord::Migration
  def change
    add_index :users, :email, unique: true
    #** This uses Rails method add_index to add an index on the email column
    #** of the users table. 
    #** the index doesn't enforce uniqueness but unique:true does
  end
end
