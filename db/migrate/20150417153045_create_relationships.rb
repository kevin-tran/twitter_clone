class CreateRelationships < ActiveRecord::Migration
  def change
    create_table :relationships do |t|
      t.integer :follower_id
      t.integer :followed_id

      t.timestamps null: false
    end
    
    add_index :relationships, :follower_id
    add_index :relationships, :followed_id
    add_index :relationships, [:follower_id, :followed_id], unique: true    
    # [ 12.1.1:1 -> user.rb] adding indexes for relationship table
    # a multiple-key index is included to enforce uniqueness on the 
    # follower_id , followed_id pairs, so that a user can't follow another user
    # more than once
    
  end
end
