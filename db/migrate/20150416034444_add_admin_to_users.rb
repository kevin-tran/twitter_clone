class AddAdminToUsers < ActiveRecord::Migration
  def change
    add_column :users, :admin, :boolean, default: false
  end
end

# [ 9.4.1:50 -> ] migration to add a boolean admin attribute to users
