class AddPasswordDigestToUsers < ActiveRecord::Migration
  def change
    add_column :users, :password_digest, :string
    #** The rails method add_column is used to add a password_digest column to
    #** users table
  end
end
