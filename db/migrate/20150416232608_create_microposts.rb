class CreateMicroposts < ActiveRecord::Migration
  def change
    create_table :microposts do |t|
      t.text :content
      t.references :user, index: true

      t.timestamps null: false
      # This line magically createsthe created_at and updated_at columns
    end
    add_foreign_key :microposts, :users
    add_index :microposts, [:user_id, :created_at]
    # 1) add_index :microposts => add index to microposts table
    # 2) B/c we expect to retrieve all microposts associated with user id in reverse
    # order of creation, we add index on user_id and created_at.
    # 3) by including both the user_id and created_at columns as an array, we 
    # arrange for Rails to create a multiple key index, which means that Active
    # record uses both keys at the same time.
  end
end

# [ 11.1.1:10-> micropost_test.rb  ] micropost migration with added index
