class AddRememberDigestToUsers < ActiveRecord::Migration
  def change
    add_column :users, :remember_digest, :string
  end
end


#             [ 8.4.1:30 -> app/models/user.rb ] Generated migration for the remember digest
# 1) Used a migration name that ends in _to_users to tell Rails tthat the migration
# is designed to alter the users table in the database.
# 2) We don’t expect to retrieve users by remember digest, there’s no need 
# to put an index on the remember_digest column, and we can use the default
# migration as generated 