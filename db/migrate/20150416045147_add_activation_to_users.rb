class AddActivationToUsers < ActiveRecord::Migration
  def change
    add_column :users, :activation_digest, :string
    add_column :users, :activated, :boolean, default: false
    add_column :users, :activated_at, :datetime
  end
end
# [ 10.1.1:2 -> user.rb ] A migrartion for account activation (with added index)
# activation is by default false